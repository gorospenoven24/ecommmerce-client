//import { useState } from 'react';
// Proptypes - use to validate props
import {ButtonGroup, ButtonToolbar} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types'
import { Row, Col, Card, CardGroup, Table, Button } from 'react-bootstrap';
import   {Link} from 'react-router-dom'
import { Container, Form} from 'react-bootstrap';
import Cart from '../pages/Cart';

export default function CartCard(cartProp){
	//console.log(props);
	const userId = localStorage.getItem('id');

	// need lang eto pag need mo icall ung property sa loob ng object
	// deconstruction of object
	const {_id, productName, quantity, price, subtotal} = cartProp;
	console.log(`cartProp ${cartProp}`);

	return (
		<Container fluid>
				
					<Table striped bordered hover size="sm">
						<thead>
							<th>Product Name</th>
							<th>Product Price</th>
							<th>Quantity</th>
							<th></th>
							<th>Sub Total</th>


							
						</thead>
					  <tbody>
					    <tr>
					     
					      <td  className="thcart">{productName}</td>
					      <td className="thprice">₱ {price}</td>
					       <td className="thprice">{quantity}</td>
					        <td>
					       	<ButtonToolbar className=" thquantity buttongroup mr-2"aria-label="Toolbar with button groups">
					       	  <ButtonGroup className=" mr-2" aria-label="Second group">
					       	    <Button>-</Button> 
					       		  </ButtonGroup>
					       		  <ButtonGroup aria-label="Third group">
					       	    <Button>+</Button>
					       	  </ButtonGroup>
					       	</ButtonToolbar>
					       </td>
					       <td className="thprice">₱ {subtotal}</td>
					      
					       
				    </tr>
				   
				  </tbody>
				</Table>
				
				
			
	
		</Container>
		)
	}

	// checks the validity of the PropTypes
CartCard.propTypes = {
	cart: PropTypes.shape({
		name: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		quantity: PropTypes.number.isRequired,
		total: PropTypes.number.isRequired,
	}) 
}
