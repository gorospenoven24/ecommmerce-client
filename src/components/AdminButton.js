import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form, tr, td, th, thead,Table, tbody} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import ButtonGroup from 'react-bootstrap/ButtonGroup'

export default function AdminButton(){

	return(
		<Container fluid className= "pl-5">
		  <Row>
		    <Col sm={3}>
		  		<ButtonGroup vertical className="buttonGroup" >
		  		  <Button className="adminButton"  as={NavLink} to="/admin">Product List</Button>
		  		  <Button className="adminButton"  as={NavLink} to="/alluser">User's List</Button>
		  		  <Button className="adminButton">Category</Button>
		  		   <Button className="adminButton"  as={NavLink} to="/allorder">Orders</Button>
		  		  <Button className="adminButton" as={NavLink} to="/logout" exact>Log out</Button>
		  		</ButtonGroup>
		    </Col>
		  </Row>
		</Container>

		)

}