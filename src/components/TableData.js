//import { useState } from 'react';
// Proptypes - use to validate props
import PropTypes from 'prop-types'
import { Row, Col, Card, CardGroup, tr, td, th, thead,Table, tbody, Button } from 'react-bootstrap';
import   {Link} from 'react-router-dom'
import { Container} from 'react-bootstrap';

export default function Product({productProp}){
	//console.log(props);
	const {_id, name, description, price, isActive} = productProp;
	return (
		<Container fluid>
			 <Table striped bordered hover size="sm">
		   
		       <tbody>
		        <tr>
		         
		          <td className = "tdName">{name}</td>
		          <td className = "tdDescription">{description}</td>
		          <td className = "tdPrice">{price}</td>
		           <td className = ""><Button varian="Primary" className="">Edit</Button></td>
		            <td className = ""><Button varian="Success" className="">Delete</Button></td>
		        </tr>
		        </tbody>
		     
		      </Table>
			
		</Container>
		)
	}

	// checks the validity of the PropTypes
	Product.propTypes = {
		// "shape" mtehod is to check if the prop object conforms to a sepecific shape
		product: PropTypes.shape({
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.string.isRequired,
			isActive: PropTypes.string.isRequired
		})
	}