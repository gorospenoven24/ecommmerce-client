import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom'
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types'
import OrderHistory from '../pages/OrderHistory'

export default function orderHistory(productProp){
	const userId = localStorage.getItem('id');
	const {productId, quantity, price, subtotal, total} = productProp;

	return (
	             

			<Container className="mt-5">
				<Card className="text-center">
				  <Card.Header>
				  <Card.Title></Card.Title>
				  </Card.Header>
				  <Card.Body>
				    <Card.Title>{productId}</Card.Title>
				    <Card.Text>
				    {productId}
				    </Card.Text>
				  </Card.Body>
				</Card>

			</Container>
		
		)
}

	// checks the validity of the PropTypes
orderHistory.propTypes = {
	cart: PropTypes.shape({
		productId: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		quantity: PropTypes.number.isRequired,
		total: PropTypes.number.isRequired,
	}) 
}
