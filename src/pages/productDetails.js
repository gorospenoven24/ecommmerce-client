import React from 'react';
import { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col, Span, Form, } from 'react-bootstrap';
import CartView from './CartView';
import { Link } from 'react-router-dom'


import ProductCard from '../components/ProductCard';



export default function ProductView(props) {
	console.log(props);
	console.log(props.match.params.productId);
	const userId = localStorage.getItem('id');
	const productId = props.match.params.productId;

	// const [name, setName] = useState([]);
	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

		/*fetch(`https://obscure-badlands-28279.herokuapp.com/products/${productId}`)*/
		// 
	useEffect(() => {
		fetch(`https://obscure-badlands-28279.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			// ang kukunin dito ay yung laman ni product model
				setProductName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				}
			);
	}, [])
	
 /*fetch(`https://obscure-badlands-28279.herokuapp.com/carts/${userId}`, {*/
	const addToCart = async () => {
       	
        	 fetch(`https://obscure-badlands-28279.herokuapp.com/carts/${userId}`, {
        	 	// method post,put, Delete ay need maglagay ng method headers and body
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            // kapag si delete kahit wala ng body
            body: JSON.stringify({
            	//eto kinukuha sa model yung mga required field
                customerId: userId,
				productId: productId,
				price: price,
				productName:productName
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(`fetch for carts${data}`);
   	    })
	}

	return(
		<Container className="mt-5" style={{width:700}}>
				<Card>
					<Card.Body >
						  <Card.Header as="h5" className="header"> 
						  		
      								<Form.Label className="productDetails">
      									{productName}
      								</Form.Label>
      						</Card.Header>
      						<Card.Subtitle as="h5" className="mt-2 ml-3"> 
						  			<Form.Label className="mr-3">
      									Description:
      								</Form.Label>
      								<Form.Label className="productDetails">
      									{description}
      								</Form.Label>
      						</Card.Subtitle>
      						<Card.Subtitle as="h5" className="mt-2 ml-3"> 
						  			<Form.Label className="mr-3">
      									Price:
      								</Form.Label>
      								<Form.Label className="productDetails">
      									{price}
      								</Form.Label>
      						</Card.Subtitle>
							<hr></hr>
							
						<Link className="btn btn-primary" variant="primary" size="lg" style={{width:620}} onClick={addToCart} to={`/`} block>Add to Cart</Link>
					</Card.Body>
				</Card>
		</Container>
	)
}
