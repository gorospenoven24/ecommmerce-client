import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form, Table, FormControl, th, td, thead,tbody,tr, ButtonToolbar,ButtonGroup} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom'
import { Redirect } from 'react-router-dom';
import Header from '../components/CartHeader'
import Footer from '../components/CartFooter'
import CartBody from '../components/CartBody'
import CartCard from '../components/CartCard'
import InputGroup from 'react-bootstrap/InputGroup'
import ProductCard from '../components/ProductCard'


// const myCart=[];
var total= 0;
export default function CartView(prop){
	console.log(prop)
	
	const productId = prop.match.params.productId;
	const cartId = prop.match.params.cartId;
	console.log(productId);


	// const [name, setName] = useState([]);
	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	// myCart=[];
	
	const [cart, setCart] = useState([]);
	const userId = localStorage.getItem('id');
		let history = useHistory();
		
	useEffect(() => {
		 fetch(`https://obscure-badlands-28279.herokuapp.com/carts/${userId}`)
		.then(res => res.json())
		.then(data => {
				setCart(data.map(function(props,index){

					  		let cartId = props._id;
					  		let productName = props.productName;
					  		let description = props.description;
					  		let price = props.price;
					  		let quantity = props.quantity;
					  		let subtotal = price* quantity;
					  		total += subtotal;

					  		const addQty = async (cart)=> {
					  				let cartId = cart.cartId;
					  			 		 fetch(`https://obscure-badlands-28279.herokuapp.com/carts/${cartId}/addQty`, {
				                		method: 'PATCH',
				                		headers:{
				                		    'Content-Type':'application/json'
				                		},
					                	})
					                	.then(res => res.json())
					                	.then(data => {
					                	document.querySelector(`#quantity-${cartId}`).innerHTML = data.quantity	
					                	})
					                	window.location.reload(false);
					  				}

					  		const minusQty = async (cart)=> {
					  				let cartId = cart.cartId;
					  			 		 fetch(`https://obscure-badlands-28279.herokuapp.com/carts/${cartId}`, {
				                		method: 'PATCH',
				                		headers:{
				                		    'Content-Type':'application/json'
				                		},
					                	})
					                	.then(res => res.json())
					                	.then(data => {
					                	document.querySelector(`#quantity-${cartId}`).innerHTML = data.quantity	
					                	})
					                	window.location.reload(false);
					  				}

					

					  		return(
					  			<Fragment>
					  				    <tr id="item-${cartId}">
					  				      <td  className="thcart">{productName}</td>
					  				      <td className="thprice">₱ {price}</td>
					  				       <td className="thprice" id="quantity-${cartId}">{quantity}</td>
					  				        <td>
					  				       	<ButtonToolbar className=" thquantity buttongroup mr-2"aria-label="Toolbar with button groups">
					  				       	  <ButtonGroup className=" mr-2" aria-label="Second group">
					  				       	   <Link  className="btn btn-success" value={cartId} onClick={()=>addQty({cartId})}>+</Link>

					  				       	    <Link  className="btn btn-warning" value={cartId} onClick={()=>minusQty({cartId})}>-</Link>

					  				       		  </ButtonGroup>
					  				       		  <ButtonGroup aria-label="Third group">
					  				       	  </ButtonGroup>
					  				       	</ButtonToolbar>
					  				       </td>
					  				       <td className="thprice">₱ {subtotal}</td>

					  			   		 </tr>

					  			   </Fragment>) 
					  			}))	
		})

	}, [])

				const Checkout = async () => {
						
						fetch(`http://localhost:4000/users/checkout`,{
					                method: 'POST',
					                headers:{
					                    'Content-Type':'application/json'
					                },
					                body: JSON.stringify({
					                	totalAmount: total,
					                	// cartId: cartId,
					                		  userId:userId,
							                  productId:productId,
							                  productName:productName


					                   
					                   

					                  
					                })
					            })
						.then(res => res.json())
						.then(data => {
							console.log(`fetch for checkout ${data}`)
					                		Swal.fire({
					                			title:"Checkout Successfully",
					                			icon:"success",
					                			text:""
					                		});
					                		
					                		history.push('/')

						})
					
					}






	return (
		<Container fluid>
				
					<Table striped bordered hover size="sm">
						<thead>
							<th>Product Name</th>
							<th>Product Price</th>
							<th>Quantity</th>
							<th></th>
							<th>Sub Total</th>
						</thead>

					  <tbody>

					  {cart}


				  </tbody>
				  <tfoot>
				  	<tr>
				  	<th>Total</th>
				  	<th></th>
				  	<th></th>
				  	<th><Link className = "btn btn-success" style={{width:300}} to={`/`}  /* onClick = {Checkout}*/>Checkout</Link></th>
				  	<th>₱ {total}</th>
				  	</tr>
			
				  </tfoot>

				</Table>
					
				
			
	
		</Container>

		)

	}


