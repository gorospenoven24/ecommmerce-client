import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form, Table, FormControl} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom'
import { Redirect } from 'react-router-dom';
import OrderHistory from '../components/OrderHistory'
import InputGroup from 'react-bootstrap/InputGroup'	


export default function CartView(){

	const [cart, setCart] = useState([]);
	const [cartItems, setCartItems] = useState([]);
	
	const userId = localStorage.getItem('id');

	useEffect(() => {
		fetch(`https://obscure-badlands-28279.herokuapp.com/carts/${userId}`)
		 // fetch(`http://localhost:4000/carts/${userId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);			
			
			setCartItems(data[0].items.map(cartItem => {
				
				return(
					<OrderHistory key={cartItem._id} productProp={cartItem} />
					)
				}
			))
			console.log(`cartItems ${cartItems}`);
			
		})
	}, [])

	return (
	             

			<Container className="mt-5">
				{cartItems}
			</Container>
		
		)
}
